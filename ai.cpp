//Rupinder :D

#include<iostream>
#include<stack>
#include<cmath>
#include<map>
#include<set>
#include<string.h>
#include<stdio.h>
#include<vector>
#include<math.h>
#include<string>
#include<algorithm>
#include<iterator>
#include<iomanip>
#include<limits.h>
#include<numeric>
using namespace std;

#define Pi 3.14159265358979323846264338327950288419716939937510582
typedef long long int lld;
typedef vector<int> vi;
typedef pair<int,int> pii;
#define sz size()
#define MOD 1000000007
#define pb push_back
#define mp make_pair
#define pf push_front
#define ppb pop_back
#define ff first
#define ss second
#define rep(i,n) for(i=0;i<n;i++)
#define all(a) a.begin(),a.end()
lld modpow(lld a,lld n,lld temp){lld res=1,y=a;while(n>0){if(n&1)res=(res*y)%temp;y=(y*y)%temp;n/=2;}return res%temp;}
lld gcd(lld a,lld b){if(a==0)return(b);else return(gcd(b%a,a));}
int solve(vi &d,int& nu1,int& nu2,int bar,int roll)
{
	vector<pair<int,int> > priority;
	int i,j,cnt=0,rank[30]={0};
	if(bar>0)
	{
		//cout<<"error!\n";
		if(d[roll-1]>=-1){
			nu1=-1;
			nu2=roll-1;
			return 1000;
		}else{
			return 0;
		}
	}else{
		for(i=0;i<d.sz;i++)
		{
			rank[i]=++cnt;
			if(i+roll<d.sz&&d[i]>0&&i<=d.sz-7){
				if(d[i+roll]==-1)
					rank[i]+=20;
				else if(d[i+roll]<-1)
					rank[i]=-1;
				if(d[i+roll]>=1)
					rank[i]+=2;
			}else{
				rank[i]=-1;
			}
			//cout<<rank[i]<<" ";
		}
		//cout<<endl;
		for(i=0;i<d.sz;i++)
			priority.pb(mp(rank[i],i));
		sort(all(priority));
		if(priority[d.sz-1].ff<0)
			return 0;
		else{
			nu1=priority[d.sz-1].ss;
			nu2=nu1+roll;
			return priority[priority.sz-1].ff;
		}
	}
}
int main()
{
	std::ios_base::sync_with_stdio( false );
	int i,j,a[13],b[13],r1,r2,bar;
	string s;
	cin>>s;
	rep(i,12)
		cin>>a[i];
	rep(i,12)
		cin>>b[i];
	cin>>r1>>r2>>bar;
	vector<pair<int,pair<int,int> > >mvrank;
	vi board;
	//calculate all possible first moves, with the ranks
	int cnt=25;
	if(s=="Bob"){
		for(i=11;i>=0;i--)
			board.pb(-a[i]);
		rep(i,12)
			board.pb(-b[i]);
		bar*=-1;
	}else{
		for(i=11;i>=0;i--)
		{	board.pb(b[i]);
			//cout<<board[board.sz-1]<<" ";
		}
		rep(i,12)
		{
			board.pb(a[i]);
			//		cout<<board[board.sz-1]<<" ";
		}
	}
	int roll1=1,roll2=1;
	if(r1<r2)
		swap(r1,r2);
	for(i=1;i<=2;i++)
	{
		if(mvrank.sz>0)
		mvrank.clear();
		int to,from,temp;
		if((temp=solve(board,from,to,bar,r1))&&roll1){
			mvrank.pb(mp(temp,mp(from,to)));
		}
		if((temp=solve(board,from,to,bar,r2))&&roll2)
		{
			mvrank.pb(mp(temp,mp(from,to)));
		}
		//cout<<(mvrank.sz>0)<<"!"<<endl;
		if(((mvrank.sz)>0))
		{
			sort(all(mvrank));
//			cout<<mvrank[mvrank.sz-1].ff<<"!\n";
			if(s=="Bob")
			{
				if(mvrank[mvrank.sz-1].ss.ff==-1)
					cout<<"Z ";
				else if(mvrank[mvrank.sz-1].ss.ff>11){
//					mvrank[mvrank.sz-1].ss.ff-=12;
					cout<<"B"<<mvrank[mvrank.sz-1].ss.ff-12+1<<" ";
				}else{
					cout<<"A"<<11-mvrank[mvrank.sz-1].ss.ff+1<<" ";
				}
				if(mvrank[mvrank.sz-1].ss.ss>11)
				{
//					mvrank[mvrank.sz-1].ss.ss-=12;
					cout<<"B"<<mvrank[mvrank.sz-1].ss.ss-12+1<<"\n";
				}else
					cout<<"A"<<11-mvrank[mvrank.sz-1].ss.ss+1<<"\n";
			}else{
				if(mvrank[mvrank.sz-1].ss.ff==-1)
					cout<<"Z ";
				else if(mvrank[mvrank.sz-1].ss.ff>11){
//					mvrank[mvrank.sz-1].ss.ff-=12;
					cout<<"A"<<mvrank[mvrank.sz-1].ss.ff-12+1<<" ";
				}else{
					cout<<"B"<<11-mvrank[mvrank.sz-1].ss.ff+1<<" ";
				}
				if(mvrank[mvrank.sz-1].ss.ss>11)
				{
//					mvrank[mvrank.sz-1].ss.ss-=12;
					cout<<"A"<<mvrank[mvrank.sz-1].ss.ss-12+1<<"\n";
				}else
					cout<<"B"<<11-mvrank[mvrank.sz-1].ss.ss+1<<"\n";
			}
			if(mvrank[mvrank.sz-1].ss.ff>=0)
				board[mvrank[mvrank.sz-1].ss.ff]--;
			else
				bar--;
			board[mvrank[mvrank.sz-1].ss.ss]++;
			if(mvrank[mvrank.sz-1].ss.ss-mvrank[mvrank.sz-1].ss.ff==r1)
				roll1=0;
			else
				roll2=0;
		}else{
			cout<<"pass\n";
		}
	}
	return 0;
}
