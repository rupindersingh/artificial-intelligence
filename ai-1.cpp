//Rupinder :D

#include<iostream>
#include<stack>
#include<cmath>
#include<map>
#include<set>
#include<string.h>
#include<stdio.h>
#include<vector>
#include<math.h>
#include<string>
#include<algorithm>
#include<iterator>
#include<iomanip>
#include<limits.h>
#include<numeric>
using namespace std;

#define Pi 3.14159265358979323846264338327950288419716939937510582
typedef long long int lld;
typedef vector<int> vi;
typedef pair<int,int> pii;
#define sz size()
#define MOD 1000000007
#define pb push_back
#define mp make_pair
#define pf push_front
#define ppb pop_back
#define ff first
#define ss second
#define rep(i,n) for(i=0;i<n;i++)
#define all(a) a.begin(),a.end()
lld modpow(lld a,lld n,lld temp){lld res=1,y=a;while(n>0){if(n&1)res=(res*y)%temp;y=(y*y)%temp;n/=2;}return res%temp;}
lld gcd(lld a,lld b){if(a==0)return(b);else return(gcd(b%a,a));}
pair<int,pii > secondmove(vi &d,int roll);
pair<pii,pii > firstmove(vi &d,int roll[])
{
	int curh=0,maxh=-INT_MAX;
	pair<int,pii> temp;
	pair<pii,pii> high=mp(mp(0,0),mp(0,0));
	if(d[0]>0)
	{
		d[0]--;
		for(int j=0;j<2;j++)
		{
			curh=0;
			if(d[roll[j]]>-1)
			{
				d[roll[j]]++;
				if(d[roll[j]]==2)
					curh+=roll[j];
				temp=secondmove(d,roll[j^1]);
				d[roll[j]]--;
				if(temp.ff+curh>maxh)
				{
					maxh=temp.ff+curh;
					if(temp.ff==-INT_MAX)
						high=mp(mp(0,0),mp(0,roll[j]));
					else
						high=mp(temp.ss,mp(0,roll[j]));
				}
			}
			else if(d[roll[j]]==-1)
			{
				curh+=24-roll[j];
				d[roll[j]]+=2;
				if(d[roll[j]]==2)
					curh+=roll[j];
				temp=secondmove(d,roll[j^1]);
				d[roll[j]]-=2;
				if(temp.ff+curh>maxh)
				{
					maxh=temp.ff+curh;
					if(temp.ff==-INT_MAX)
						high=mp(mp(0,0),mp(0,roll[j]));	
					else
						high=mp(temp.ss,mp(0,roll[j]));
				}
			}
		}
		d[0]++;
		//return best moves
	}else{

		for(int i=1;i<=24;i++)
		{
			curh=0;
			if(d[i]>0)
			{
				if(d[i]==2)
					curh-=i;
				d[i]--;
			}else
				continue;
			for(int j=0;j<2;j++)
			{
				curh=0;
				if(d[i+roll[j]]>-1){
					d[i+roll[j]]++;
					if(d[i+roll[j]]==2)
						curh+=i+roll[j];
					temp=secondmove(d,roll[j^1]);
					d[i+roll[j]]--;
					if(temp.ff+curh>maxh)
					{
						maxh=temp.ff+curh;
						if(temp.ff==-INT_MAX)
							high=mp(mp(0,0),mp(i,i+roll[j]));
						else
							high=mp(temp.ss,mp(i,i+roll[j]));
					}
				}else if(d[i+roll[j]]==-1){
					curh+=24-(i+roll[j]);//cut
					d[i+roll[j]]+=2;
					if(d[i+roll[j]]==2)
						curh+=i+roll[j];//pair
					temp=secondmove(d,roll[j^1]);
					d[i+roll[j]]-=2;
					if(temp.ff+curh>maxh)
					{
						maxh=temp.ff+curh;
						if(temp.ff==-INT_MAX)
							high=mp(mp(0,0),mp(i,i+roll[j]));
						else
							high=mp(temp.ss,mp(i,i+roll[j]));
					}
				}
			}
			d[i]++;
		}
	}
	return high;
}
pair<int,pii > secondmove(vi &d,int roll)
{
	int curh=0;
	if(d[0]>0)
	{
		if(d[roll]<-1)
			return mp(-INT_MAX,mp(0,0));
		else{
			if(d[roll]==1)
				curh+=roll;
			if(d[roll]==-1)
				curh+=24-roll;
			return mp(curh,mp(0,roll));
		}
	}else{
		int maxh=-INT_MAX,p1,p2;
		for(int i=1;i<=24;i++)
		{
			curh=0;
			if(d[i]>0&&d[i+roll]>=-1)
			{
				//calc health
				if(d[i]==2)
					curh-=i;
				if(d[i+roll]==-1)
					curh+=24-(i+roll);
				if(d[i+roll]==1)
					curh+=i;
				if(curh>maxh)
				{
					maxh=curh;
					p1=i;
					p2=i+roll;
				}
			}
		}
		return mp(maxh,mp(p1,p2));
	}
}
int main()
{
	std::ios_base::sync_with_stdio( false );
	int i,j,a[33],b[33],roll[3],bar;
	string s;
	cin>>s;
	rep(i,12)
		cin>>a[i];
	rep(i,12)
		cin>>b[i];
	cin>>roll[0]>>roll[1]>>bar;
	vi board;
	//calculate all possible first moves, with the ranks
	int cnt=25;
	board.pb(bar);
	if(s=="Bob"){
		char aux[100];
		int k=1;
		for(i=11;i>=0;i--)
		{
			board.pb(-a[i]);
			sprintf(aux,"A%d",i);
			strcpy(output[k],aux);
			k++;
		}
		k=13;
		rep(i,12)
		{
			board.pb(-b[i]);
			sprintf(aux,"B%d",i);
			strcpy(output[k],aux);
			k++;
		}
		bar*=-1;
	}else{
		char aux[100];
		int k=1;
		for(i=11;i>=0;i--)
		{	board.pb(b[i]);
			sprintf(aux,"B%d",i);
			strcpy(output[k],aux);
			//cout<<board[board.sz-1]<<" ";
		}
		k=1;
		rep(i,12)
		{
			board.pb(a[i]);
			sprintf(aux,"A%d",k);
			strcpy(output[k],aux);
			//		cout<<board[board.sz-1]<<" ";
		}
	}
	if(roll[0]<roll[1])
		swap(roll[0],roll[1]);
	pair<pii,pii> fmove;
	fmove=firstmove(board,roll);
	if(fmove.ff.ff==0&&fmove.ff.ss==0)
		cout<<"pass\n";
	else
		cout<<fmove.ff.ff<<" "<<fmove.ff.ss<<"\n";
	if(fmove.ss.ff==0&&fmove.ss.ss==0)
		cout<<"pass\n";
	else
		cout<<fmove.ss.ff<<" "<<fmove.ss.ss<<"\n";
	return 0;
}
