Sources: None
Assumptions: 
1)one cannot move any other checker unless the checkers on the bar have been cleared.
2)a checker from the bar cannot displace a lone checker at any point.
Algorithm:
Associate with each possible move, a value(utility/heuristic) and choose the maximum among the neighbouring states. The heuristic contains a factor of distance from home board, the possibility of hitting opponent's blot, and making own blot invulnerable.It is a kind of local search. 
