//Rupinder :D

#include<iostream>
#include<stack>
#include<cmath>
#include<map>
#include<set>
#include<string.h>
#include<stdio.h>
#include<vector>
#include<math.h>
#include<string>
#include<algorithm>
#include<iterator>
#include<iomanip>
#include<limits.h>
#include<numeric>
#include<float.h>
using namespace std;

#define Pi 3.14159265358979323846264338327950288419716939937510582
typedef long long int lld;
typedef vector<int> vi;
typedef pair<int,int> pii;
#define sz size()
#define MOD 1000000007
#define pb push_back
#define mp make_pair
#define pf push_front
#define ppb pop_back
#define ff first
#define ss second
#define rep(i,n) for(i=0;i<n;i++)
#define all(a) a.begin(),a.end()
lld modpow(lld a,lld n,lld temp){lld res=1,y=a;while(n>0){if(n&1)res=(res*y)%temp;y=(y*y)%temp;n/=2;}return res%temp;}
lld gcd(lld a,lld b){if(a==0)return(b);else return(gcd(b%a,a));}
pair<float,pii > secondmove(vi &d,int roll);
pair<pii,pii > firstmove(vi &d,int roll[])
{
	bool bear=1;
	for(int i=0;i<=18;i++)
		if(d[i]>0)
			bear=0;
	float curh=0,maxh=-FLT_MAX;
	float toth;
	pair<float,pii> temp;
	pair<pii,pii> high=mp(mp(0,0),mp(0,0));
	if(bear){
		for(int j=0;j<2;j++)
		{
			bool empty=25-roll[j],behind=0;
			for(int i=19;i<=24;i++)
			{
				curh=0;
				if(!empty ||(empty&&behind)){
					if(i>25-roll[j]||d[i]<=0)continue;
					if(i+roll[j]==25)
						curh+=25;
					else if(d[i+roll[j]]==1)
						curh+=i+roll[j];
					else if(d[i+roll[j]]==-1)
						curh+=24-(i+roll[j]);
					else if(d[i+roll[j]]==0)
						curh-=(i+roll[j]);
					if(d[i+roll[j]]>1)
						curh+=i+roll[j];
					if(d[i]>1)
						curh-=i;
					if(d[i]==2)
						curh-=2*i;
					if(curh>maxh){
						curh=maxh;
						if(j==0)
						{
							high.ff.ff=i;
							high.ff.ss=i+roll[j];
						}else{
							high.ss.ff=i;
							high.ss.ss=i+roll[j];
						}
					}
				}else if(empty&&(!behind))
				{
					if(d[i]<=0)continue;
					if(d[i]==2)
						curh-=i;
					if(d[i]==1)
						curh+=i;
					if(curh>maxh){
						curh=maxh;
						if(j==0)
						{
							high.ff.ff=i;
							high.ff.ss=i+roll[j];
						}else{
							high.ss.ff=i;
							high.ss.ss=i+roll[j];
						}
					}
				}
			}

		}

	}else if(d[0]>0)
	{
		d[0]--;
		for(int j=0;j<2;j++)
		{
			curh=0;
			if(d[roll[j]]>-1)
			{
				d[roll[j]]++;
				if(d[roll[j]]==2)
					curh+=roll[j];
				else if(d[roll[j]]>2)
					curh+=roll[j]/2;
				temp=secondmove(d,roll[j^1]);
				d[roll[j]]--;
				if(temp.ff==-FLT_MAX)
					toth=curh;
				else
					toth=curh+temp.ff;
				if(toth>maxh)
				{
					maxh=toth;
					if(temp.ff==-FLT_MAX)
						high=mp(mp(0,roll[j]),mp(0,0));
					else
						high=mp(mp(0,roll[j]),temp.ss);
				}
			}
			else if(d[roll[j]]==-1)
			{
				curh+=24-roll[j];
				d[roll[j]]+=2;
				temp=secondmove(d,roll[j^1]);
				d[roll[j]]-=2;
				if(temp.ff==-FLT_MAX)
					toth=curh;
				else
					toth=curh+temp.ff;
				if(toth>maxh)
				{
					maxh=toth;
					if(temp.ff==-FLT_MAX)
						high=mp(mp(0,roll[j]),mp(0,0));	
					else
						high=mp(mp(0,roll[j]),temp.ss);
				}
			}
		}
		d[0]++;
		//return best moves
	}else{

		for(int i=1;i<=24;i++)
		{
			curh=0;
			if(i>6&&i<=18)
				curh+=i/5;
			else if(i<=6)
				curh+=i/10;
			if(d[i]>0)
			{
				if(d[i]==2)
					curh-=i;
				else if(d[i]>2)
					curh-=i/2;
				d[i]--;
			}else
				continue;
			float temph=curh;
			for(int j=0;j<2;j++)
			{
				curh=temph;
				if(i+roll[j]>24)continue;
				if(d[i+roll[j]]>-1){
					d[i+roll[j]]++;
					if(d[i+roll[j]]==2)
						curh+=i+roll[j];
					else if(d[i+roll[j]]>2)
						curh+=(i+roll[j])/2;
					temp=secondmove(d,roll[j^1]);
					d[i+roll[j]]--;
					if(temp.ff==-FLT_MAX)
						toth=curh;
					else
						toth=curh+temp.ff;
					if(toth>maxh)
					{
						//cout<<"hi"<<i<<toth<<"\n";
						maxh=toth;
						if(temp.ff==-FLT_MAX)
							high=mp(mp(i,i+roll[j]),mp(0,0));
						else
							high=mp(mp(i,i+roll[j]),temp.ss);
					}
				}else if(d[i+roll[j]]==-1){
					curh+=24-(i+roll[j]);//cut
					d[i+roll[j]]+=2;
					temp=secondmove(d,roll[j^1]);
					d[i+roll[j]]-=2;
					if(temp.ff==-FLT_MAX)
						toth=curh;
					else
						toth=curh+temp.ff;
					if(toth>maxh)
					{
						maxh=toth;;
						if(temp.ff==-FLT_MAX)
							high=mp(mp(i,i+roll[j]),mp(0,0));
						else
							high=mp(mp(i,i+roll[j]),temp.ss);
					}
				}
			}
			d[i]++;
		}
	}
	return high;
}
pair<float,pii > secondmove(vi &d,int roll)
{
	float curh=0;
	if(d[0]>0)
	{
		if(d[roll]<-1)
			return mp(-FLT_MAX,mp(0,0));
		else{
			if(d[roll]==1)
				curh+=roll;

			else if(d[roll]>1)
				curh+=roll/2;
			if(d[roll]==-1)
				curh+=24-roll;
			return mp(curh,mp(0,roll));
		}
	}else{
		float maxh=-FLT_MAX;
		int p1,p2;
		for(int i=1;i<=24;i++)
		{
			curh=0;
			if(i>6&&i<=18)
				curh+=i/5;
			if(d[i]>0&&d[i+roll]>=-1&&i+roll<=24)
			{
				//calc health
				if(d[i]==2)
					curh-=i;
				else if(d[i]>2)
					curh-=i/2;
				if(d[i+roll]==-1)
					curh+=24-(i+roll);
				if(d[i+roll]==1)
					curh+=i+roll;
				else if(d[i+roll]>1)
					curh+=(i+roll)/2;
				if(curh>maxh)
				{
					maxh=curh;
					p1=i;
					p2=i+roll;
				}
			}
		}
		return mp(maxh,mp(p1,p2));
	}
}
int main()
{
	std::ios_base::sync_with_stdio( false );
	int i,j,a[33],b[33],roll[3];
	string s,bar;
	cin>>s;
	rep(i,24)
		cin>>a[i];
	cin>>roll[0]>>roll[1];
	vi board;
	//calculate all possible first moves, with the ranks
	int cnt=25;
	
	board.pb(0);
	if(roll[0]<roll[1])
		swap(roll[0],roll[1]);
	pair<pii,pii> fmove;
	fmove=firstmove(board,roll);
	char aux1[5],aux2[5];
	int temp;
	if(fmove.ss.ff==0&&fmove.ss.ss!=0)
		swap(fmove.ff,fmove.ss);
	//cout<<fmove.ff.ff<<" "<<fmove.ff.ss<<"\n";
	//cout<<fmove.ss.ff<<" "<<fmove.ss.ss<<"\n";
	if(fmove.ff.ff==0&&fmove.ff.ss==0)
		cout<<"pass\n";
	else
	{
		if (s=="Alice")
		{
			if(fmove.ff.ff<=12&&fmove.ff.ff>0)
			{
				temp=12-fmove.ff.ff+1;
				sprintf(aux1,"B%d",temp);
			}
			else if (fmove.ff.ff>=13)
			{
				temp=fmove.ff.ff-12;
				sprintf(aux1,"A%d",temp);
			}
			if(fmove.ff.ss<=12&&fmove.ff.ss>0)
			{
				temp=12-fmove.ff.ss+1;
				sprintf(aux2,"B%d",temp);
			}
			else if (fmove.ff.ss>=13)
			{
				temp=fmove.ff.ss-12;
				sprintf(aux2,"A%d",temp);
			}
			if(fmove.ff.ff==0)
				strcpy(aux1,"Z");
			if(fmove.ff.ss==0)
				strcpy(aux2,"Z");
		}
		else
		{
			if(fmove.ff.ff<=12&&fmove.ff.ff>0)
			{
				temp=12-fmove.ff.ff+1;
				sprintf(aux1,"A%d",temp);
			}
			else if (fmove.ff.ff>=13)
			{
				temp=fmove.ff.ff-12;
				sprintf(aux1,"B%d",temp);
			}
			if(fmove.ff.ss<=12&&fmove.ff.ss>0)
			{
				temp=12-fmove.ff.ss+1;
				sprintf(aux2,"A%d",temp);
			}
			else if (fmove.ff.ss>=13)
			{
				temp=fmove.ff.ss-12;
				sprintf(aux2,"B%d",temp);
			}
			if(fmove.ff.ff==0)
				strcpy(aux1,"Z");
			if(fmove.ff.ss==0)
				strcpy(aux2,"Z");
		}
		//	cout<<fmove.ff.ff<<" "<<fmove.ff.ss<<"\n";
		cout<<aux1<<" "<<aux2<<"\n";
	}
	if(fmove.ss.ff==0&&fmove.ss.ss==0)
		cout<<"pass\n";
	else
	{
		if (s=="Alice")
		{
			if(fmove.ss.ff<=12&&fmove.ss.ff>0)
			{
				temp=12-fmove.ss.ff+1;
				sprintf(aux1,"B%d",temp);
			}
			else if (fmove.ss.ff>=13)
			{
				temp=fmove.ss.ff-12;
				sprintf(aux1,"A%d",temp);
			}
			if(fmove.ss.ss<=12&&fmove.ss.ss>0)
			{
				temp=12-fmove.ss.ss+1;
				sprintf(aux2,"B%d",temp);
			}
			else if (fmove.ss.ss>=13)
			{
				temp=fmove.ss.ss-12;
				sprintf(aux2,"A%d",temp);
			}
			if(fmove.ss.ff==0)
				strcpy(aux1,"Z");
			if(fmove.ss.ss==0)
				strcpy(aux2,"Z");
		}
		else
		{
			if(fmove.ss.ff<=12&&fmove.ss.ff>0)
			{
				temp=12-fmove.ss.ff+1;
				sprintf(aux1,"A%d",temp);
			}
			else if (fmove.ss.ff>=13)
			{
				temp=fmove.ss.ff-12;
				sprintf(aux1,"B%d",temp);
			}
			if(fmove.ss.ss<=12&&fmove.ss.ss>0)
			{
				temp=12-fmove.ss.ss+1;
				sprintf(aux2,"A%d",temp);
			}
			else if (fmove.ss.ss>=13)
			{
				temp=fmove.ss.ss-12;
				sprintf(aux2,"B%d",temp);
			}
			if(fmove.ss.ff==0)
				strcpy(aux1,"Z");
			if(fmove.ss.ss==0)
				strcpy(aux2,"Z");
		}
		//cout<<fmove.ss.ff<<" "<<fmove.ss.ss<<"\n";
		cout<<aux1<<" "<<aux2<<"\n";
	}
	return 0;
}
